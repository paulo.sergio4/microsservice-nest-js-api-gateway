import { ApiProperty } from "@nestjs/swagger";

export class UserDto {
	id?: string;

	@ApiProperty({
		description: 'O nome é utilizado para identificar o usuário com acesso ao sistema',
		example: 'Qualquer texto digitável que contenha nome próprio- (Paulo Sérgio, paulo sergio)'
	})
	name: string;

	@ApiProperty({
		description: 'O email é utilizado para identificar um usuário com acesso ao sistema e para campos de buscas',
    example: 'email@example.com',
	})
	email: string;

	@ApiProperty({
		description: 'Caracterizado por uma cadeia de caracteres numéricos aplicados a uma string',
    example: 'algarismos em formato de texto (88)988888888',
	})
	phone: string;

	@ApiProperty({
		description: 'A senha autentica um usuário cadastrado a ter acesso ao sistema',
    example: 'tipos de texto ADRFGTHH123456assdfsdfsc¨&/$%-%"!@',
	})
	password: string;
}
