import { Body, Controller, Get, OnModuleInit, Post } from '@nestjs/common';
import { Client, ClientKafka, Transport } from '@nestjs/microservices';
import { ApiBody } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { UserDto } from './dto/users.dto';

@Controller('users')
export class UsersController implements OnModuleInit {
	@Client({
		transport: Transport.KAFKA,
		options: {
			client: {
				clientId: 'users',
				brokers: ['kafka:9092'],
			},
			/*consumer: {
				groupId: 'user-consumer',
				allowAutoTopicCreation: true
			}*/
		}
	})

	private client: ClientKafka;

	async onModuleInit() {
		const requestPattern = [
			'find-all-user',
			'create-user'
		]

		requestPattern.forEach(async pattern => {
			this.client.subscribeToResponseOf(pattern);
			await this.client.connect()
		})
	}

	@Get()
	@ApiBody({ type: UserDto })
	listusers(): Observable<UserDto[]> {
		return this.client.send('find-all-user', {})
	}

	@Post()
	@ApiBody({ type: UserDto })
	create(@Body() user: UserDto): Observable<UserDto> {
		return this.client.emit('create-user', user)
	}
}
