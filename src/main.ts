import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const port = process.env.PORT;

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	//app.startAllMicroservices()
	app.enableCors();
	const config = new DocumentBuilder()
		.setTitle('API - Gateway')
		.setDescription('Gateway to connect microsservices API')
		.setVersion('1.0')
		.addTag('app')
		.build();
	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup('api', app, document);

	await app.listen(port);
}
bootstrap();
